package tests;

import model.Person;

public class Test01 {
	public static void main(String[] args) {
		System.out.println("EXAMPLE 1");
		Person person1 = new Person("Mario");
		Person anotherPerson1 = person1;
		doSomething1(person1);
		System.out.println(person1.getName());
		System.out.println(anotherPerson1.getName());
		System.out.println("");
		System.out.println("EXAMPLE 2");
		Person person2 = new Person("Giovanni");
		Person anotherPerson2 = person2;
		doSomething2(person2);
		System.out.println(person2.getName());
		System.out.println(anotherPerson2.getName());
		
	}
	
	public static void doSomething1(Person person) {
		System.out.println(person.getName());
		person = new Person("Giuseppe");
		System.out.println(person.getName());
	}
	
	public static void doSomething2(Person person) {
		System.out.println(person.getName());
		person.setName("Luca");
		System.out.println(person.getName());
	}
}
