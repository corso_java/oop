package model;

public class Student extends Person {

	private String schoolCode;

	public Student(String name, String surname, int age, String schoolCode) {
		super(name, surname, age);
		this.schoolCode = schoolCode;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	@Override
	public String toString() {
		return super.toString()+" Student [schoolCode=" + schoolCode + "]";
	}
	
	

}
